<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TodoListItem;

class TodoList extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        array_push($this->appends,'items', 'complete_rate');
    }

    public function getItemsAttribute(){
    	$items = TodoListItem::where('todo_list_id', $this->id)->get();
      	return $items;
    }

    public function getCompleteRateAttribute(){
    	if ($this->items->count() > 0) {
    		return number_format($this->items->where('completed', true)->count() / $this->items->count() * 100, 0);
    	}
    }

}

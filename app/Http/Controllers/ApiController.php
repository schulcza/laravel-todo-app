<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\TodoList;
use App\TodoListItem;

class ApiController extends Controller
{
	/**
	 * Get Todo List
	 */
	public function getLists() {
		$todoList = TodoList::where('user_id', \Auth::user()->id)->get();

		return \Response::json($todoList);
	}

	/**
	 * Get list item for a todo list 
	 */	
	public function getListItems(Request $request) {
		$listItems = TodoList::find($request->list_id);

		return \Response::json($listItems->items);
	}

	/**
	 * Delete todo list
	 */
	public function deleteList(Request $request) {
		$todoList = TodoList::find($request->input('list_id'));

		if ($todoList) {
			$todoList->delete();

			$todoListItems 	= TodoListItem::where('todo_list_id', $request->input('list_id'))->get();
			foreach ($todoListItems as $key => $todoListItem) {
				$todoListItem->delete();
			}
		}
	}

	/**
	 * Delete todo list item
	 */
	public function deleteItem(Request $request) {
		$todoListItem 		= TodoListItem::find($request->input('item_id'));

		if ($todoListItem) {
			$todoListItem->delete();
		}

		return \Response::json($todoListItem);
	}

	/**
	 * Save Todo list
	 */

	public function saveList(Request $request) {
		\Log::debug(print_r($request->all(), true));
		$validatedData = $request->validate([
	        'title' => 'required|max:255',
	    ]);

	    if ($request->input('id')) {
	    	$list = TodoList::find($request->input('id'));
	    } else {
	    	$list = new TodoList();	    	
	    }

	    $list->title 	= $request->input('title');
	    $list->user_id 	= \Auth::user()->id;
	    $list->save();

	    return \Response::json($list);
	}
	
	/**
	 * Save todo list item
	 */
	public function saveListItem(Request $request) {
		$validatedData = $request->validate([
	        'title' => 'required|max:255',
	    ]);

	     if ($request->input('id')) {
	    	$item = TodoListItem::find($request->input('id'));
	    } else {
	    	$item = new TodoListItem();	    	
	    }
	    $item->title 		= $request->input('title');
	    $item->description	= $request->input('description');
	    $item->todo_list_id	= $request->input('list_id');
	    $item->save();

	    return \Response::json($item);
	}

	/**
	 * Update todo list item to complete/uncomplete
	 */
	public function completeItem(Request $request) {
	    $item 				= TodoListItem::find($request->input('item.id'));
	    $item->completed 	= $item->completed == false ? true : false;
	    $item->save();

	    return \Response::json($item);
	}

	/**
	 * Get logged in user data
	 */
	public function getUser() {
    	$user = \Auth::user();
    	
	    return \Response::json($user);
    }

    /**
     * Save user data
     */
    public function saveUser(Request $request) {
		$validator = \Validator::make($request->all(), [
	        'user.name' => 'required|max:255',
	        'user.email' => 'required|max:255',
	    ]);

	    if ($validator->fails()) {
            $response = [
		    	'code' => 400,
		    	'messages' => $validator->messages(),
		    ];

	    	return \Response::json($response);
        }

	    $user 				= User::find($request->input('user.id'));

	    $user->name 		= $request->input('user.name');
	    $user->email		= $request->input('user.email');
	    $user->birthdate	= date('Y-m-d', strtotime($request->input('user.birthdate')));
	    $user->save();

	    $response = [
	    	'user' => $user,
	    	'code' => 200,
	    	'messages' => ['Sikeres mentés'],
	    ];

	    return \Response::json($response);
	}

	/**
	 * Change user password
	 */
	public function changePassword(Request $request) {
		$validator = \Validator::make($request->all(), [
	        'user.email' 				=> 'required',
	        'password.old_password' 	=> 'required|max:255',
	        'password.password'			=> 'required|confirmed|max:255',
	    ]);

	    $user = User::where('email', $request->input('user.email'))->first();

	    if ($user) {
		    if (!\Hash::check($request->input('password.old_password'), $user->password)) {
			 	$response = [
			    	'code' => 400,
			    	'messages' => ['Invalid password'],
			    ];

		    	return \Response::json($response);
			}
	    


		    if ($validator->fails()) {
	            $response = [
			    	'code' => 400,
			    	'messages' => $validator->messages(),
			    ];

		    	return \Response::json($response);
	        }

		    $user->password = \Hash::make($request->input('password.password'));
		    $user->save();

		    $response = [
		    	'user' => $user,
		    	'code' => 200,
		    	'messages' => ['Sikeres mentés'],
		    ];
	    } else {
	    	$response = [
		    	'code' => 400,
		    	'messages' => ['User not found.'],
		    ];
	    }

	    return \Response::json($response);
	}
}

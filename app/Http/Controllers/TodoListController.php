<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TodoList;
use App\TodoListItem;

class TodoListController extends Controller
{
	/**
	 * Render Todo List page
	 */
	public function index() {
		return view('todos.list');
	}

}

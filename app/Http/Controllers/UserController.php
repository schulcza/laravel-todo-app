<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
	/**
	 * Rendes User settings Page
	 */
    public function settings() {
    	return view('settings');
    }
}

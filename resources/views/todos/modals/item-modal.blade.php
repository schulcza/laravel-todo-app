<div class="modal fade" id="itemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="listModal"><% item.id ? 'Edit' : 'Create' %> item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <div class="modal-body">
                    <input name="title" class="form-control" ng-model="item.id" type="text" hidden/>
                    <div class="form-group">                                
                        <label for="">Title</label>
                        <input name="title" class="form-control" ng-model="item.title" type="text" value=""/>
                    </div>
                    <div class="form-group">                                
                        <label for="">Description</label>
                        <textarea name="description" class="form-control" cols="30" rows="10" ng-model="item.description"></textarea>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" ng-click="saveItem(item)" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
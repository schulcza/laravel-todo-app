<div class="modal fade" id="listModal" tabindex="-1" role="dialog" aria-labelledby="listModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="listModal"><% list.id ? 'Edit' : 'Create' %> list</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <div class="modal-body">
                        
                        <input name="title" class="form-control" ng-model="list.id" type="text" hidden/>
                        <div class="form-group">                                
                            <label for="">Title</label>
                            <input name="title" class="form-control" ng-model="list.title" type="text" value=""/>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" ng-click="saveList(list)" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
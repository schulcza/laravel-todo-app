@extends('layouts.app')

@section('content')
<div class="container" ng-controller="TodoController" ng-cloak>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
					<div class="row">
						<div class="" ng-class="{'col-md-4':selectedList, 'col-md-12': !selectedList}">
							<div class="box box-aqua">
								<div class="box-header">
									<i class="ion ion-clipboard"></i>
									<h3 class="box-title">To Do Lists</h3>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-default pull-right" ng-click="addList()" data-toggle="modal" data-target="#listModal"><i class="fa fa-plus"></i> Add list</button>
									</div>
								</div>

								<div class="box-body">
									<ul class="todo-list">
										<li ng-repeat="list in lists" ng-click="selectList(list)" ng-class="{'active': selectedList.id == list.id}">
											<span class="text"><b><% list.title %> <% list.complete_rate ? '(' + list.complete_rate + '%)' : '' %></b></span>
											<div class="tools">
												<i ng-click="editList(list); $event.stopPropagation();" class="fa fa-edit fa-lg"></i>
												<i ng-click="deleteList(list); $event.stopPropagation();" class="fa fa-trash-o fa-lg"></i>
											</div>
											<div>
												<small class="label label-danger"><i class="fa fa-clock-o"></i><% list.created_at %></small>
											</div>
											<span ng-show="list.complete_rate" class="progress" ng-style="{ 'width': list.complete_rate + '%' }"></span>
										</li>
									</ul>
									<div class="empty-text" ng-show="lists.length <= 0">
										<span>No ToDo list</span>
										<span>Create one!</span>
										<span>
											<button type="button" ng-click="addList()" class="btn btn-default" data-toggle="modal" data-target="#listModal"><i class="fa fa-plus"></i> Add list</button>
										</span>
									</div>
								</div>
								<div class="box-footer clearfix no-border">
									
								</div>
							</div>
						</div> 

						<div class="col-md-8" ng-show="selectedList">

							<div class="box box-aqua">
								<div class="box-header">
									<i class="ion ion-clipboard"></i>
									<h3 class="box-title">List items</h3>
									<div class="box-tools pull-right">
										<button ng-show="selectedList" ng-click="addItem()" type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#itemModal"><i class="fa fa-plus"></i> Add item</button>
									</div>
								</div>

								<div class="box-body">
									<ul class="todo-list" ng-show="items.length > 0">
										<li ng-repeat="item in items" ng-click="selectItem(item.id);">
											<input type="checkbox" ng-click="checkItem(item);  $event.stopPropagation();" ng-model="item.completed" ng-checked="item.completed == 1" value="1" name="">
											<span class="text" ng-class="{'completed' : item.completed}"><b><% item.title %></b></span>
											<div class="tools">
												<i ng-click="editItem(item); $event.stopPropagation();" class="fa fa-edit fa-lg"></i>
												<i ng-click="deleteItem(item); $event.stopPropagation();" class="fa fa-trash-o fa-lg"></i>
											</div>
											<div>
												<small class="label label-danger"><i class="fa fa-clock-o"></i><% item.created_at %></small>
											</div>
											<div class="description" ng-show="showDescriptionId == item.id">
												<span class="text"><% item.description %></span>
											</div>
										</li>
									</ul>
									<div class="empty-text" ng-show="items.length <= 0">
										<span>Empty list</span>
										<span>
											<button type="button" ng-click="addItem()" class="btn btn-default" data-toggle="modal" data-target="#itemModal"><i class="fa fa-plus"></i> Add item</button>
										</span>
									</div>
								</div>
								<div class="box-footer clearfix no-border">
									<button type="button" ng-click="closeListItems()" class="btn btn-default pull-right" >Close</button>
								</div>
							</div>
						</div> 
					</div>
                </div>
            </div>
        </div>
    </div>
	@include('todos.modals.list-modal')
	@include('todos.modals.item-modal')
</div>
@endsection

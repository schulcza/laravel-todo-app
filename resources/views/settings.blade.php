@extends('layouts.app')

@section('content')
<div class="container" ng-controller="UserController" ng-cloak>
    <div class="row justify-content-center">
        <div class="col-md-8">
        	<div class="alert alert-success" ng-show="messages.length > 0">
        		<ul>
        			<li ng-repeat="message in messages">
        				<% message %>
        			</li>
        		</ul>
			</div>
            <div class="card">
                <div class="card-body">
					<div class="box box-aqua">
						<div class="box-header">
							<h3 class="box-title">User settings</h3>
						</div>

						<div class="box-body">
                    		<input name="title" class="form-control" ng-model="user.id" type="text" hidden/>
							<div class="form-group">								
								<label for="">Name</label>
                    			<input class="form-control" ng-model="user.name" type="text" value=""/>
							</div>
							<div class="form-group">								
								<label for="">E-mail</label>
                    			<input class="form-control" ng-model="user.email" type="text" value=""/>
							</div>
							<div class="form-group">								
								<label for="">Birthdate</label>
                    			<input class="form-control" ng-model="user.birthdate" type="date" value=""/>
							</div>
						</div>
						<div class="box-footer clearfix no-border pull-right">
                			<button type="submit" ng-click="saveUser(user)" class="btn btn-primary">Save changes</button>
						</div>
					</div>
                </div>
                <div class="card-body">
					<div class="box box-aqua">
						<div class="box-header">
							<h3 class="box-title">Change password</h3>
						</div>
						<div class="box-body">
							<div class="form-group">								
								<label for="">Old password</label>
                    			<input class="form-control" ng-model="password.old_password" type="password" value=""/>
							</div>
							<div class="form-group">								
								<label for="">New password</label>
                    			<input class="form-control" ng-model="password.password" type="password" value=""/>
							</div>
							<div class="form-group">								
								<label for="">Confirm new password</label>
                    			<input class="form-control" ng-model="password.password_confirmation" type="password" value=""/>
							</div>
						</div>
						<div class="box-footer clearfix no-border pull-right">
                			<button type="submit" ng-click="changePassword(user, password)" class="btn btn-primary">Change password</button>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

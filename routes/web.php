<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function() {
	Route::get('/', 'TodoListController@index');
	Route::get('/settings', 'UserController@settings');

	Route::group(['prefix' => 'lists'], function() {
		Route::get('/get', 'ApiController@getLists');
		Route::post('/items', 'ApiController@getListItems');
		Route::post('/item/complete', 'ApiController@completeItem');
		Route::post('/item/delete', 'ApiController@deleteItem');
		Route::post('/delete', 'ApiController@deleteList');
		Route::post('/items/save', 'ApiController@saveListItem');
		Route::post('/save', 'ApiController@saveList');
	});

	Route::group(['prefix' => 'user'], function() {
		Route::post('/password/change', 'ApiController@changePassword');
		Route::get('/get', 'ApiController@getUser');
		Route::post('/save', 'ApiController@saveUser');
	});

		
});

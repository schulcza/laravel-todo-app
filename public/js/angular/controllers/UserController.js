app.controller('UserController', ['$scope', '$rootScope', '$filter', '$http', '$timeout', function ($scope, $rootScope, $filter, $http, $timeout, $anchorScroll) {

	$scope.message = '';

	$scope.getUserData = function() {
		$http({
			url: '/user/get',
			method: 'GET',
		}).then(function (response) {
			$scope.user = response.data;
			$scope.user.birthdate = new Date(response.data.birthdate);
		});
	}

	$scope.getUserData();

    $scope.saveUser = function(user) {
    	$scope.messages = [];
        $http({
            url: '/user/save',
            method: 'POST',
            data: {
                user: user
            }
        }).then(function (response) {
        	if (response.data.code != 200) {
        		angular.forEach(response.data.messages, function(message, key) {
				  $scope.messages.push(message[0]);
				});
        	} else {
	        	$scope.messages.push(response.data.messages[0]);
        	}
        });
    }

    $scope.changePassword = function(user, password) {
    	$scope.messages = [];
        $http({
            url: '/user/password/change',
            method: 'POST',
            data: {
                user: user,
                password: password,
            }
        }).then(function (response) {
        	if (response.data.code != 200) {
        		angular.forEach(response.data.messages, function(message, key) {
				  $scope.messages.push(message[0]);
				});
        	} else {
	        	$scope.messages.push(response.data.messages[0]);
        	}
        	
        });
    }

   
}]);

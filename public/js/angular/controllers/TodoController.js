var app = angular.module('todo', [

],function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('TodoController', ['$scope', '$rootScope', '$filter', '$http', '$timeout', function ($scope, $rootScope, $filter, $http, $timeout, $anchorScroll) {

	$scope.getLists = function() {
		$http({
			url: '/lists/get',
			method: 'GET',
		}).then(function (response) {
			$scope.lists = response.data;
		});
	}

	$scope.getLists();

	$scope.selectList = function(list) {
        $http({
            url: '/lists/items',
            method: 'POST',
            data: {
                list_id: list.todo_list_id ? list.todo_list_id : list.id
            }
        }).then(function (response) {
            $scope.selectedList = list;
            $scope.items = response.data;
        });
    }

    $scope.closeListItems = function() {
        delete $scope.selectedList;
    }

    $scope.editList = function(list) {
		$('#listModal').modal('show');
		$scope.list = list;
    }

    $scope.addList = function() {
		delete $scope.list;
    }

    $scope.deleteList = function(list) {
        $http({
            url: '/lists/delete',
            method: 'POST',
            data: {
                list_id: list.id
            }
        }).then(function (response) {
  			delete $scope.items;
  			
        	var index = $scope.lists.indexOf(list);
  			$scope.lists.splice(index, 1);

  			$scope.getLists();
        });
    }

    $scope.deleteItem = function(item) {
        $http({
            url: '/lists/item/delete',
            method: 'POST',
            data: {
                item_id: item.id
            }
        }).then(function (response) {
        	var index = $scope.items.indexOf(item);
  			$scope.items.splice(index, 1);

  			$scope.getLists();
        });
    }

    $scope.selectItem = function(itemId) {
    	if ($scope.showDescriptionId == itemId) {
    		delete $scope.showDescriptionId;
    	} else {
        	$scope.showDescriptionId = itemId;    		
    	}
    }

    $scope.editItem = function(item) {
		$('#itemModal').modal('show');
		$scope.item = item;
    }

    $scope.addItem = function() {
		delete $scope.item;
    }

    $scope.saveList = function(list) {
        $http({
            url: '/lists/save',
            method: 'POST',
            data: {
                id: list.id,
                title: list.title,
            }
        }).then(function (response) {
        	if (!list.id) {
            	$scope.lists.push(response.data);
        	}
            $('#listModal').modal('hide');
            $scope.selectList(response.data);
        });
    }

    $scope.checkItem = function(item) {
    	$http({
            url: '/lists/item/complete',
            method: 'POST',
            data: {
                item: item
            }
        }).then(function (response) {
            item = response.data;
            $scope.getLists();
        });
    }

    $scope.saveItem = function(item) {
        $http({
            url: '/lists/items/save',
            method: 'POST',
            data: {
                id: item.id,
                title: item.title,
                description: item.description,
                list_id: $scope.selectedList.id
            }
        }).then(function (response) {
        	if (!item.id) {
            	$scope.items.push(response.data);
        	}
            $('#itemModal').modal('hide');

            $scope.getLists(response.data.todo_list_id);
        });
    }

   
}]);
